
## Crossref Labs Reports

![Image](https://crossref.org/img/labs/creature1.svg)
### Warnings, Caveats and Weasel Words

This is a [Crossref Labs](https://www.crossref.org/labs/) version of our production [Particpation reports](https://www.crossref.org/members/prep/).

Trust it as you would a scorpoion sitting on your nose.

🤥🦂

### Why have we done this?

We made a [big splash](https://www.crossref.org/blog/321-its-lift-off-for-participation-reports/) with Crossref Participation Reports in 2018 after research with our community. At that time, the aim of providing these reports was to demonstrate best practice for our members’ metadata records, and give, for the first time: 

> Clear visualization of the metadata that Crossref has. Search for any member to find out what percentage of their content includes 10 key elements of information, above and beyond the basic bibliographic metadata that all members are obliged to provide. This includes metadata such as ORCID iDs for contributors, funding acknowledgements, reference lists, and abstracts—richer metadata that makes content more discoverable, and much more useful to the scholarly community as a whole, including among members themselves.

And it worked! Having a user-interface layer over our [REST API](https://www.crossref.org/documentation/retrieve-metadata/rest-api/) to show, simply and openly, the metadata our members were registering with us helped provide clarity for those members and the wider community, and in doing so incentivised best practices and participation in key metadata fields. 

Metadata, like research, and life, does not stand still (and nor should it). So we’ve known for a while that we want to evolve Participation Reports to reflect up-to-date best practice for members, along with incorporating feedback on the existing reports.

Top of the list of requests were:
- Can we have more information about the member included in the reports? 
- Can we compare one member against another in the reports? 
- The report says I’m only registering references for 90% of my current journal articles, can you point me towards the records I’m missing so I can figure out why? 
- Can you show me what I’ve registered quarterly so that I can match that against my content registration invoice? 
- Can you add more metadata fields as you add them to your schema e.g. ROR, or different content types e.g. grants? 

Combined with this, Crossref keeps gathering new and interesting data that we want to share with our community and could provide a useful lens into the members e.g. the website domains/URLs where their DOIs resolve. So having a place where we can put data from our experiments makes this easier, as does developing them in the open, through our Labs/R&D team, so that we can get early feedback and iterate.

### Why doesn't it do X?

We're glad you asked. The [source is available in GitLab](https://gitlab.com/crossref/labs/crossref-labs-reports) under an MIT Open Source License. We even built it with an eye toward extension. It uses standard libraries like [pandas](https://pandas.pydata.org/) and [Plotly](https://plotly.com/) to do most of its work. And the enitre page is driven by the most excellent [Streamlit](https://streamlit.io/). We look forward to your merge requests!

### How often is the data updated?

When we feel like it. If there is demand, we can do this more automatically, and more frequently. The last time the data was updated in noted at the top of the sidebar.

### Can you move the widgets 12 pixels to the right?

Probably not. Streamlit is fantastic, but it doesn't give you fine-grained control over all UI elements.

### Like it? Don’t like it? Like bits of it? 
The reason we’re sharing this beta version with you now is that we need feedback. Please!

You can use [Gitlab Issues](https://gitlab.com/crossref/labs/crossref-labs-reports/-/issues) report bugs, write up suggestions, and share your thoughts. Thank you!

