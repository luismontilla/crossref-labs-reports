POLITE_HEADERS = {'User-Agent': 'Crossref Precipitation Reports; mailto:labs@crossref.org'}

CR_PRIMARY_COLORS = {
    "CR_PRIMARY_RED": "#ef3340",
    "CR_PRIMARY_GREEN": "#3eb1c8",
    "CR_PRIMARY_YELLOW": "#ffc72c",
    "CR_PRIMARY_LT_GREY": "#d8d2c4",
    "CR_PRIMARY_DK_GREY": "#4f5858",
}

COVERAGE_CATEGORY_LABELS = {
    "abstracts": "Abstracts",
    "affiliations": "Affiliations",
    "award-numbers": "Award Numbers",
    "descriptions": "Descriptions",
    "funders": "Funder Registry IDs",
    "licenses": "Licenses",
    "open-references": "Open References",
    "orcids": "ORCID IDs",
    "references": "References",
    "resource-links": "Text mining URLs",
    "ror-ids": "ROR Identifiers",
    "similarity-checking": "Similarity Check URLs",
    "update-policies": "Crossmark enabled",
}

PERIODS = ["All", "Current", "Backfile"]

API_URI = "https://api.crossref.org"

COUNTS_COL_PREFIX = 'counts-type-all'

HEADERS={'User-Agent':'Crossref Precipitation Reports; mailto:labs@crossref.org'}

QUARTERLY_DEPOSIT_DIR = "data/quarterlies"