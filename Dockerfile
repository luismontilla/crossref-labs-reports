# syntax=docker/dockerfile:1
FROM python:3.10-slim
RUN apt-get update; apt-get install -y curl unzip gcc python3-dev
WORKDIR /code
# set virtual env
ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
# RUN od -An -N1 -i /dev/random > random_num

COPY requirements.txt requirements.txt
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
EXPOSE 5000
ARG CACHE_DATE=2023-06-03
RUN apt-get install curl &&  curl -SL "https://gitlab.com/crossref/api-summaries/-/jobs/artifacts/main/download?job=annotate" -o artifacts.zip && unzip artifacts.zip
RUN rm data/*.json
RUN rm data/*.db
RUN rm data/*.csv
RUN ls data/
RUN echo $GIT_REVISION > REVISION
# RUN ls
COPY . .
# NB turn off file watcher for streamlit because iwatch doesn't like working with so many
# files in the data directory in the container
CMD ["streamlit", "run","labs-reports.py","--server.fileWatcherType","none"]