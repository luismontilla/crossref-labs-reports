### How to use it


- Select one or more of your favourite Crossref members.
- Choose the record type you would like to focus on (default is the most common record type for the selected members).
- Choose the period of time you want to focus on (all, current, back-year)


### A few tips

- You can select multiple members and compare them.
- You will see "+" symbols scattered around the page. These expand to show more detail.
- When you are looking at detail in a table, you can click on the header of a column to sort by that column.

### Help us make it better

Please [create a new issue](https://gitlab.com/crossref/labs/crossref-labs-reports/-/issues) to make suggestions or report problems.
