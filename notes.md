## Notes


### TODO

- Update parquet files in the background.
- ~Add ability to link to a particular report by passing in paramters.~
- ~Last date of updates~
- ~Add content-type breakdowns to overview~
- Any way to identify sponsors?
- List prefixes
- Accounting ID (associated with Sponsor)
- System ID
- Change name
- Leader-board
- Date ranges (how do these interact with current/backfile?). Issue here is that we need to precomute this. Would be good to understand use-case better. Would breakdown by year be useful. Or maybe- in addtion to current/backfile we have year-to-date and last-30 days?
- add [count of DOIs to guages](https://stackoverflow.com/questions/42450028/showing-total-on-stacked-bar-plotly) 
- finish adding missing metadata links 
- ~fix for members who have no DOIs~
- put approximate total
- calculate local equivalent fees
- Sentry logging
- Prometheus metrics

### Docker

To work around caching and always force download of latest API summaries:

`docker-compose build --build-arg CACHE_DATE="$(date +%s)"`

### fly

To work around cache and force update only if on new day:

`flyctl deploy . --build-arg CACHE_DATE="$(date)"`

To work around cache and always force update to latest API summaries:

`flyctl deploy . --build-arg CACHE_DATE="$(date +%s)"`

To restart:

`flyctl restart prep`


## Updating data

In Billybob project...

```
./download_latest_member_list.sh
```

Set the following env variables:
```
CR_ADMIN_USR
CR_ADMIN_ROLE
CR_ADMIN_PWD
```
Then download latest member billling metadata (this takes 15-20 minutes):

```
python get_member_billing_metadata.py --verbose --members-file members.json --results-dir billing_metadata
```

Then generate the needed reports for each prefix. This can take up to 4 hours:

```
time python generate_billing_files.py --verbose --quarter Q2 --proxy "socks5h://localhost:8123" --results-dir billing_metadata
[2] 0:zsh*
```