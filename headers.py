import logging
import json
import typer
from pathlib import Path
from typing import Optional
from rich.logging import RichHandler
import pandas

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def main(
    parquet_file: Optional[Path] = typer.Option(
        ..., exists=True, file_okay=True, dir_okay=False, readable=True
    ),
   
    verbose: bool = False,):
    logger.info("Reading parquet file")
    df = pandas.read_parquet(parquet_file)
    print(json.dumps(sorted(df.columns.values.tolist()),indent=4))
    logger.info("Done")
    


if __name__ == "__main__":
    typer.run(main)
