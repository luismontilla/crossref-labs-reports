import sys
import logging
import typer
from pathlib import Path
from typing import Optional
import shutil

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def get_all_metadata_files(dir):
    return list(dir.rglob("metadata.json"))


def get_all_quarterly_summaries(dir):
    return list(dir.rglob("all.parquet"))


def get_all_paths(dir, fn):
    return list(dir.rglob(fn))


def copy_files(src_path, dst_path):
    Path(dst_path).mkdir(parents=True, exist_ok=True)
    logger.info(f"{src_path} -> {dst_path}")
    shutil.copy(src_path, dst_path)


def main(
    src_dir: Optional[Path] = typer.Option(
        ..., exists=True, file_okay=False, dir_okay=True, readable=True
    ),
    dst_dir: Optional[Path] = typer.Option(
        ..., exists=True, file_okay=False, dir_okay=True, readable=True
    ),
    verbose: bool = False,
):

    logger.info("Updating quarterly data")

    metadata_files = get_all_paths(src_dir, "metadata.json")
    for mdf in metadata_files:
        member_id, fn = mdf.parts[-2:]
        dst_path = dst_dir / "quarterlies" / member_id
        copy_files(mdf, dst_path)

    quarterly_summaries = get_all_paths(src_dir, "all.parquet")
    for qfn in quarterly_summaries:
        member_id, period, fn = qfn.parts[-3:]
        dst_path = dst_dir / "quarterlies" / member_id / period
        copy_files(qfn, dst_path)
    logger.info("Done")


if __name__ == "__main__":
    typer.run(main)
